// http protocole module
const http = require('http');
// o outro ficheiro q fiz
const app = require('./app');
//
// cree un port
const portOk = val => {
    const port = parseInt(val, 10);
    // ça parse le "val"
    // parseInt = convertie une chaine de caracteres en integer
    // le 2eme param, 10, ? (pas compris)
    if (isNaN(port)) {
        return val;
        // ça envoie un bolean
        // si c'est pas un nombre ça exectute (et envoie "val")
        // sinon continue au prochain if
    }
    //
    if (port >= 0) {
        // si le port est un numero positif
        return port;
    }
    //
    return false;
    // si rien ne marche, ça stope le programme
}
//
//
//
// un error handler sert a s'occuper des erreurs 
const errorHandler = error => {
    // error = parametre
    if (error.syscall !== 'listen') {
        throw error;
        // throw = ça nous envoie l'erreur et arrete le programme
        // 
    }
    //
    const address = server.address();
    // ça retourne les infos de l'addresse  
    const bind = typeof address === 'string' ? 'pipe ' + address : 'port: ' + port;
    // ternaire
    //si l'addresse est un string, si c'est "pipe" ça nous donne l'adresse,
    // si c'est "port" ça nous donne le port
    //
    switch (error.code) {
        // un switch pour les erreurs communs
        case 'EACCESS':
            console.error(bind + 'Permission denied');
            process.exit(1);
            // 1 c'est un des plusiors exit existents
            // celui la envoie un code d'erreur
            break;
        case 'EADDRINUSE':
            console.error(bind + 'Adress already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }

}
//
const server = http.createServer(app);
//
server.on('error', errorHandler);
// cette methode est activé au moment ou le server demarre (on)
// la methode est lancé s'il y a un erreur
//
server.on ('listening', () => {
    // la methode esy activé quand le server ecoute le port
    const address = server.address(); 
    console.log('Listen on port ' + port);
})
//
const port = portOk(process.env.PORT || 3000);
app.set('port', port);
//
//
//
server.listen(port);
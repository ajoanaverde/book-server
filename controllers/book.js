const Book = require('../models/Book');
//
////////////
// CREATE//
//////////
//
exports.createBook = (req, res) => {
    const book = req.body
    const bookOk = new Book({
        ...book
    })
    console.log(bookOk);
    bookOk.save().then(result => {
        res.status(201).json({
            message: "Book crée"
        })
    }).catch(err => {
    res.status(400).json({message: "Une erreur est survenue"})
})
}
//
////////////
//  GET  //
//////////
//
exports.getBooks = (req, res) => {
    Book.find().then(books => {
        res.status(200).json(books);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
//////////////////
//  GET BY ID  //
////////////////
//
exports.getBookById = (req, res) => {
    Book.findById(req.params.id).then(book => {
        res.status(200).json(book);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
///////////////
//  UPDATE  //
/////////////
//
exports.updateBook = (req, res) => {
    const bookUpdated = new Book({
        ...req.body
    })
    Book.findByIdAndUpdate(req.params.id, bookUpdated).then(result => {
        res.status(201).json({message: "Le book a ete mis a jour"})
    }).catch(err => {
        res.status(400).json(err)
    })
}
//
///////////////
//  DELETE  //
/////////////
//
exports.deleteBook = (req, res) => {
    Book.findByIdAndDelete(req.params.id).then(() => {
        res.status(201).json({message: "Le book a ete suprime"})
    }).catch(err => {
        res.status(400).json(err)
    })
}
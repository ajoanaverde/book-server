//
const express = require('express');
// express sert pour les routes et les middlewhere
// ça sert a etablir la comunication au sein d'une application
const cors = require('cors');
const app = express();
// ici on fait de notre apli une apli express
const bodyParser = require('body-parser')
//
const mongoose = require('mongoose');
//
const booksController = require('./controllers/book');
//
mongoose.connect('mongodb://localhost:27017/aula', {
    useNewUrlParser: true
}).then(() => {
    console.log("Connection mongodb ok")
}).catch(err => {
    console.log(err)
})
//
app.use(cors());
//
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
//
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});
//
// ROUTES
// attencion : l'ordre des routes est importante
//
app.delete('/books/:id', booksController.deleteBook);
app.put('/books/:id', booksController.updateBook);
app.get('/books/:id', booksController.getBookById);
app.post('/books', booksController.createBook);
app.get('/books', booksController.getBooks);
//
//
// ça exporte le fichier pour pouvoir l'utiliser ailleurs 
module.exports = app;
//
//
// dans une api il y a pas des vues
// un mvc classique, les 3 parties sont emsemble 
// una api, le front et le back sont completement separes
// une api est aussi plus secure
//
// web service:
// 